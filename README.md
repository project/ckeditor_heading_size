# CKEditor heading size
This module provides a context menu for heading tags so you can specify a font size.

## INSTALLATION

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

* Configure the font size options at `/admin/config/content/ckeditor-heading-size`.

### DEVELOPMENT
In the module directory, run `yarn install` to set up the necessary assets. After installing dependencies, the plugin can be built with `yarn build` or `yarn
watch`. They will be built to `js/build/ckeditorHeadingSize.js`.
